/opt/zabbix/scripts/ora-sysctl-check.sh:
  file.managed:
    - user: zabbix
    - group: zabbix
    - mode: 644
content_ora-sysctl-check.sh:
  file.append:
    - name: /opt/zabbix/scripts/ora-sysctl-check.sh
    - text: |
        #!/bin/bash
        #####
        #laoragis0i1
        #####
        ls /etc/sysctl.d/97-oracle-database-sysctl.conf > /dev/null 2>&1
        echo $?

/etc/zabbix/zabbix_agentd.d/userparams.conf:
   cmd.run:
     - name: echo 'UserParameter=ora-db-sysctl-status,/opt/zabbix/scripts/ora-sysctl-check.sh' >> /etc/zabbix/zabbix_agent2.d/userparams.conf
zabbix-agent2:
  service.running:
    - restart: True
