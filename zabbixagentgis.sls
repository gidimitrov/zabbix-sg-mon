/opt/zabbix/scripts/clusterstatus.sh:
  file.managed:
    - user: zabbix
    - group: zabbix
    - mode: 744
content_clusterstatus.sh:
  file.append:
    - name: /opt/zabbix/scripts/clusterstatus.sh
    - text: |
        #!/bin/bash
        ###exit status###
        #Severity status down           1       critical
        #Severity status unknown        2       critical
        #Severity status starting       3       warning
        #Severity state halted          4       warning
        #Severity state failed          5       critical
        #Severity state unknown         6       critical
        #Severity state reforming       7       warning
        #Severity auto_run disabled     8       warning
        ###Vars###
        cmd=/usr/local/cmcluster/bin/cmviewcl
        log=/var/log/zabbix/cluster_status
        ###
        ###Chech if cluster was halted from os side.
        if (sudo tail -100 /var/log/messages |grep 'Stopping LVM2');then
            date >> $log
            sudo grep -C2 'Stopping LVM2' /var/log/messages >> $log
            echo "Node halted not gracefully" >> $log
            exit 9
        fi
        if (date +%T|egrep '^17') && (date +%w|egrep '6') || (date +%T|egrep '^18') && (date +%w|egrep '6') ;then
            date >> $log
            echo "Backup job is startting." >> $log
            $cmd -f table -l package >> $log
            exit 0
        else
         if [ -f /nsr/ptausioffline_CV ]; then
            date >> $log
            echo "Backup job is running" >> $log
            exit 0
         elif  ($cmd -f table -l package|grep 'maintenance');then
            date >> $log
            $cmd -f table -l package|grep 'maintenance' >> $log
            echo "Cluster package is in maintenance." >> $log
            exit 0
         else
                 if $cmd |awk '{print $2}'|grep 'down'; then
                 date >> $log
                 echo 'Severity status down' >> $log
                 $cmd -f table -l package|grep 'down'  >> $log
                 exit 1

                 elif $cmd |awk '{print $2}'|grep 'unknown'; then
                 date >> $log
                 echo 'Severity status unknown' >> $log
                 $cmd -f table -l package|grep 'unknown'  >> $log
                 exit 2

                 elif  $cmd |awk '{print $2}'|grep 'starting';then
                 date >> $log
                 echo 'Severity status starting '
                 $cmd -f table -l package|grep 'starting'  >> $log
                 exit 3

                 elif  $cmd |awk '{print $3}'|grep 'halted';then
                 date >> $log
                 echo 'Severity state halted' >> $log
                 $cmd -f table -l package|grep 'halted'  >> $log
                 exit 4

                 elif  $cmd |awk '{print $3}'|grep 'failed';then
                 date >> $log
                 echo 'Severity state failed' >> $log
                 $cmd -f table -l package|grep 'failed'  >> $log
                 exit 5

                 elif  $cmd |awk '{print $3}'|grep 'unknown';then
                 date >> $log
                 echo 'Severity state unknown'   >> $log
                 $cmd -f table -l package|grep 'unknown'  >> $log
                 exit 6

                 elif  $cmd |awk '{print $3}'|grep 'reforming';then
                 date >> $log
                 echo 'Severity state reforming' >> $log
                 $cmd -f table -l package|grep 'reforming'  >> $log
                 exit 7

                 elif  $cmd |awk '{print $4}'|grep 'disabled';then
                 date >> $log
                 echo 'Severity auto_run disabled' >> $log
                 $cmd -f table -l package|grep 'disabled'  >> $log
                 exit 8

                 else
                 date >> $log
                 echo 'Cluster is in good state' >> $log
                 exit 0


                 fi
            fi
          fi
/opt/zabbix/scripts/status.sh:
   file.managed:
     - user: zabbix     
     - group: zabbix
     - mode: 744
content_status.sh:
   file.append:
     - name: /opt/zabbix/scripts/status.sh
     - text: |
         #!/bin/bash
         log=/var/log/zabbix/cluster_status
         /opt/zabbix/scripts/clusterstatus.sh >> log
         echo $?
/etc/zabbix/zabbix_agentd.d/userparams.conf:
   file.managed:    
     - user: root
     - group: root
     - mode: 644
content_userparams.conf:
  file.append:
    - name: /etc/zabbix/zabbix_agentd.d/userparams.conf
    - text: |
        UserParameter=systemd.unit.is-active[*],systemctl is-active --quiet '$1' && echo 1 || echo 0
        UserParameter=systemd.unit.is-failed[*],systemctl is-failed --quiet '$1' && echo 1 || echo 0
        UserParameter=systemd.unit.is-enabled[*],systemctl is-enabled --quiet '$1' && echo 1 || echo 0
        UserParameter=failed.logins[*],journalctl --since "$1 minutes ago" _SYSTEMD_UNIT=sshd.service | egrep -c "[Ff]ailed|[Ff]ailure|[Ii]nvalid"
        UserParameter=failed.sudo[*],journalctl --since "$1 minutes ago" | egrep -c "user NOT in sudoers"
        UserParameter=check.secupdates,/opt/zabbix/scripts/check_security_updates.sh
        UserParameter=check.fsro,/opt/zabbix/scripts/check_ro.sh
        UserParameter=read.users,/opt/zabbix/scripts/read_user.sh
        UserParameter=custom.meminfo.query[*],grep ^$1: /proc/meminfo |awk '{print $$2}'
        UserParameter=custom.vfs.discover_disks,/opt/zabbix/scripts/lld-disks.py
        UserParameter=custom.vfs.dev.read.ops[*],awk '{print $$1}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.read.merged[*],awk '{print $$2}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.read.sectors[*],awk '{print $$3}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.read.ms[*],awk '{print $$4}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.write.ops[*],awk '{print $$5}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.write.merged[*],awk '{print $$6}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.write.sectors[*],awk '{print $$7}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.write.ms[*],awk '{print $$8}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.io.active[*],awk '{print $$9}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.io.ms[*],awk '{print $$10}' /sys/class/block/$1/stat
        UserParameter=custom.vfs.dev.weight.io.ms[*],awk '{print $$11}' /sys/class/block/$1/stat
        UserParameter=custom.ntp.discover_peers,/opt/zabbix/scripts/lld_ntppeers.sh
        UserParameter=custom.ntp.offset[*],ntpq -pn |awk 'NR>2' |grep '^.$1' |awk '{print $$9}'
        UserParameter=log.check[*],journalctl --since "$1 minutes ago" -t $2 | egrep -c -i "$3"
        UserParameter=read.mp,/opt/zabbix/scripts/read_mp.sh
        UserParameter=check.mpro[*],sudo rm "$(sudo mktemp -p $1 zabbix_rw_test.XXXXXX)" && echo $?
        UserParameter=clusterstatus,/opt/zabbix/scripts/status.sh	
zabbix-agent:
  service.running:
    - restart: True    

