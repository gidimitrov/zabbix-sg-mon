echo:
  cmd.run:
    - creates:
      - /etc/logrotate.d/clusterstatus
apply:
  cmd.run:
    - name: /usr/bin/echo -e '/var/log/zabbix/cluster_status {'  > /etc/logrotate.d/clusterstatus
/etc/logrotate.d/clusterstatus:
  file.append:
    - text: |
        maxsize 300k
        missingok
        notifempty
        rotate 4
        compress
        create 0644 zabbix zabbix
        }

